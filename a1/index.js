	console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	

	function printFullName () {
			let fullName = prompt("What is your name?", "");
			console.log ("Hello, " + fullName);
		}

	printFullName ();

	function printAge () {
			let age = prompt("How old are you?", "");
			console.log ("You are " + age + " years old.");
		}

	printAge ();

	function printLocation () {
			let location = prompt("Where do you live?", "");
			console.log ("You live in " + location);
		}

	printLocation ();


	alert("Thank you for your input!");


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printTop5Bands () {
		console.log("1. Parokya ni Edgar");
		console.log("2. Panic at the Disco");
		console.log("3. Arctic Monkeys");
		console.log("4. Hozier");
		console.log("5. Franco");
	}

	printTop5Bands ();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printTop5Movies () {
		console.log("1. I Origins");
		console.log("Rotten Tomatoes Rating: 69%");
		console.log("2. The Prestige");
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("3. The Big Short");
		console.log("Rotten Tomatoes Rating: 88%");
		console.log("4. Submarine");
		console.log("Rotten Tomatoes Rating: 82%");
		console.log("5. Everything Everywhere All at Once");
		console.log("Rotten Tomatoes Rating: 89%");
	}

	printTop5Movies ();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	
	let printFriends = function printUsers () {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	};
	printFriends ();

// console.log(friend1);
// console.log(friend2);