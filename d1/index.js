// printName ();

console.log("Hello World");

// Functions
	// Functions in Javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	// Function declarations
		// (function statement) defines a function with the specified parameters

		/*
			Syntax:

			function functionName () {
				code block (statement)
			}
		*/


		// function keyword - used to define a JavaScript function
		// functionName - the function name. Functions are named to be able to use later in the code.
		// function blocl ({}) - the statements which comprise the body of the function. This is where the code is to be executed.
		// We can assign a variable to hold a function, but that will be explained later


		function printName () {;
			console.log ("My name is John. ")
		}

		printName ();

		// semicolons are used to separate executable JavaScript statements.

		// Function Invocation
			// the code block and statements inside a function is not immediately executed when the function is defined. The code block


		printName ();

		// declaredFunction(); //error //much like variables, we cannot invoke a function we have yet to define

		declaredFunction();

		function declaredFunction (){
			console.log ("Hello World from declaredFunction")
		}


		// variableFunction(); //error

		/*
		error - function expressions, being stored in a let or const variable cannot be hoisted
		*/

		let variableFunction = function() {
			console.log("Hello Again");
		}
		
		variableFunction ();

		// we can also create a function expression of a named function
		// However, to invoke the function expression, we invoke it by its variable name, not by its function name
		// Function expressions are always invoked (called) using the variable name


		let funcExpression = function funcName () {
			console.log("Hello From the Other Side");
		};

		// funcName();
		funcExpression();

		// you can reassing declared functions and funtion expression to new anonymous functions


		declaredFunction = function(){
			console.log("updated declaredFunction")
		};

		declaredFunction();

		funcExpression = function (){
			console.log("updated funcExpression")
		}

		funcExpression ();

		const constantFunc = function(){
			console.log("Initialized with const!");
		};
		constantFunc();

		// constantFunc = function(){
		// 	console.log("Cannot be re-assigned!");
		// };
		// constantFunc(); // error

		// Function Scoping



		{
			let localVar = "Armando Perez";
			  console.log(localVar)

		}

		let globalVar = "Mr. Worldwide";
		console.log(globalVar);
		// console.log(localVar); // error



		function showNames() {
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}

			showNames ();


			// console.log(functionVar); 
			// console.log(functionConst);
			// console.log(functionLet);

			// // error when called outside the function


		// Nested Functions

		function myNewFunction() {
			let name = "Cee"

				function nestedFunction(){
					let nestedName = "Thor";
					console.log(name);
					console.log(nestedName);
				}
				// console.log(nestedName);
				// results to an error
				// nestedName is not defined
				nestedFunction();
		};
		myNewFunction();



		// let globalName = "Nej";

		// myNewFunction2();


		// Using alert()

		// alert()allows us to show a small window at the top of our browser page to show information to our users
		// as opposed to our console.log() which only show the message on the console
		// It allows us to show a short dialog or instruction to our user.
		// the page will await until the user dismisses the dialog

		alert("Hello World");

		// alert() syntax
		// alert("<messageInString");
		// you can do it in numbers too

		// You can also use an alert() to show a message to the user from a later function invocation


		function showSampleAlert (){
			alert("Hello User!")
		};

		showSampleAlert ();

		console.log("I will only log in the console whent the alert is dismissed");

		// notes on the use of alert();
			// Show only an alert() for short dialogs/messages for the user
			// do not overuse alert() becaue the program/js has to wait for it to be dismissed before continuing


		// Using prompt ()

			// promtp() allows us to show a small window at the top of our browser to gather user input
			// It, much like alert() ,will have the page wait until the user completes or  enters their input
			// the input from the prompt() will be returned as a String once the user dismisses the window

			let samplePrompt = prompt("Enter your name.")
			console.log("Hello," + samplePrompt);


			let sampleNullPrompt = prompt("Don't enter anything");
			console.log(sampleNullPrompt);

			// prompt() returns an empty string when there is no input
			// null if the user cancels the prompt()


			// prompt()  used globally will be run immediately, so for better user experience, it is much better to use them accordingly or add them inside a function

			function printWelcomeMessage(){
				let firstName = prompt("Enter Your First Name");
				let lastName = prompt("Enter Your Last Name");
				console.log("Hello " + firstName + lastName)
				console.log("Welcome to my page!")
			}

			printWelcomeMessage ();


			// Function Naming Conventions

			// function names should be definitive if the task it will perform, it usually contains a verb

			function getCourses(){
				let courses = ["Science 101", "Math 101", "English 101"]
				console.log(courses);
			};

			getCourses();
			getCourses(1);

			// Avoid generic names to avoid confusion within your code


			function get () {
				let name = "Jamie";
				console.log(name);

			};

			get();